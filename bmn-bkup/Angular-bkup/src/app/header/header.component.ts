import { Component, PLATFORM_ID, Inject, OnInit } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})


export class HeaderComponent implements OnInit {
  constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

  ngOnInit(): void {
    // Check if running in a browser environment
    if (isPlatformBrowser(this.platformId)) {
      const indicator = document.querySelector('.nav-indicator') as HTMLElement | null;
      const items = document.querySelectorAll('.nav-item');
      

      function handleIndicator(el: HTMLElement) {
        items.forEach(item => {
          item.classList.remove('is-active');
          item.removeAttribute('style');
        });

        if (indicator) {
          indicator.style.width = `${el.offsetWidth}px`;
          indicator.style.left = `${el.offsetLeft}px`;
          indicator.style.backgroundColor = el.getAttribute('active-color') || '';
        }

        el.classList.add('is-active');
        el.style.color = el.getAttribute('active-color') || '';
      }
      

      items.forEach((item, index) => {
        item.addEventListener('click', (e) => { handleIndicator(e.target as HTMLElement); });
        item.classList.contains('is-active') && handleIndicator(item as HTMLElement);
      });
    }
  }


}
