package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Repository.RegistrationRepository;
import com.model.login;

@Service
public class RegistrationService {
    
    private final RegistrationRepository repository;

    @Autowired
    public RegistrationService(RegistrationRepository repository) {
        this.repository = repository;
    }
    
    public login saveFooduser(login user){
        return repository.save(user);    
    }

    public login fetchUserByEmaiId(String EmailId){
        return repository.findByEmailId(EmailId);
    }
    
    public login fetchUserByEmailIdAndPassword(String EmailId , String Password){
        return repository.findByEmailIdAndPassword(EmailId,Password);
    }
}