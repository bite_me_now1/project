package com.ts.Bitemenow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EntityScan(basePackages="com.model")
@SpringBootApplication(scanBasePackages = "com")
@ComponentScan("com.Repository")

public class BitemenowApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitemenowApplication.class, args);
	}

}